defmodule AntonWeb.PageControllerTest do
  use AntonWeb.ConnCase

  setup %{conn: conn} do
    params = %{name: "test", email: "test@example.com", password: "test", password_confirmation: "test"}
    user = Anton.Coherence.Schemas.change_user(params)
    |> Anton.Repo.insert!
    {:ok, conn: assign(conn, :current_user, user), user: user}
  end

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "Hello Anton!"
  end
end
