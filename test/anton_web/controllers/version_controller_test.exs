defmodule AntonWeb.VersionControllerTest do
  use AntonWeb.ConnCase

  alias Anton.Releases

  @create_attrs %{name: "some name", notes: "some notes"}
  @update_attrs %{name: "some updated name", notes: "some updated notes"}
  @invalid_attrs %{name: nil, notes: nil}

  setup %{conn: conn} do
    params = %{name: "test", email: "test@example.com", password: "test", password_confirmation: "test"}
    user = Anton.Coherence.Schemas.change_user(params)
    |> Anton.Repo.insert!
    {:ok, conn: assign(conn, :current_user, user), user: user}
  end

  def fixture(:version) do
    {:ok, version} = Releases.create_version(@create_attrs)
    version
  end

  describe "index" do
    test "lists all versions", %{conn: conn} do
      conn = get conn, version_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Versions"
    end
  end

  describe "new version" do
    test "renders form", %{conn: conn} do
      conn = get conn, version_path(conn, :new)
      assert html_response(conn, 200) =~ "New Version"
    end
  end

  describe "create version" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, version_path(conn, :create), version: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == version_path(conn, :show, id)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, version_path(conn, :create), version: @invalid_attrs
      assert html_response(conn, 200) =~ "New Version"
    end
  end

  describe "edit version" do
    setup [:create_version]

    test "renders form for editing chosen version", %{conn: conn, version: version} do
      conn = get conn, version_path(conn, :edit, version)
      assert html_response(conn, 200) =~ "Edit Version"
    end
  end

  describe "update version" do
    setup [:create_version]

    test "redirects when data is valid", %{conn: conn, version: version} do
      conn = put conn, version_path(conn, :update, version), version: @update_attrs
      assert redirected_to(conn) == version_path(conn, :show, version)
    end

    test "renders errors when data is invalid", %{conn: conn, version: version} do
      conn = put conn, version_path(conn, :update, version), version: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Version"
    end
  end

  describe "delete version" do
    setup [:create_version]

    test "deletes chosen version", %{conn: conn, version: version} do
      conn = delete conn, version_path(conn, :delete, version)
      assert redirected_to(conn) == version_path(conn, :index)
    end
  end

  defp create_version(_) do
    version = fixture(:version)
    {:ok, version: version}
  end
end
