defmodule Anton.ReleasesTest do
  use Anton.DataCase

  alias Anton.Releases

  describe "versions" do
    alias Anton.Releases.Version

    @valid_attrs %{name: "some name", notes: "some notes"}
    @update_attrs %{name: "some updated name", notes: "some updated notes"}
    @invalid_attrs %{name: nil, notes: nil}

    def version_fixture(attrs \\ %{}) do
      {:ok, version} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Releases.create_version()

      version
    end

    test "list_versions/0 returns all versions" do
      version = version_fixture()
      assert Releases.list_versions() == [version]
    end

    test "get_version!/1 returns the version with given id" do
      version = version_fixture()
      assert Releases.get_version!(version.id) == version
    end

    test "create_version/1 with valid data creates a version" do
      assert {:ok, %Version{} = version} = Releases.create_version(@valid_attrs)
      assert version.name == "some name"
      assert version.notes == "some notes"
    end

    test "create_version/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Releases.create_version(@invalid_attrs)
    end

    test "update_version/2 with valid data updates the version" do
      version = version_fixture()
      assert {:ok, version} = Releases.update_version(version, @update_attrs)
      assert %Version{} = version
      assert version.name == "some updated name"
      assert version.notes == "some updated notes"
    end

    test "update_version/2 with invalid data returns error changeset" do
      version = version_fixture()
      assert {:error, %Ecto.Changeset{}} = Releases.update_version(version, @invalid_attrs)
      assert version == Releases.get_version!(version.id)
    end

    test "delete_version/1 deletes the version" do
      version = version_fixture()
      assert {:ok, %Version{}} = Releases.delete_version(version)
      assert_raise Ecto.NoResultsError, fn -> Releases.get_version!(version.id) end
    end

    test "change_version/1 returns a version changeset" do
      version = version_fixture()
      assert %Ecto.Changeset{} = Releases.change_version(version)
    end
  end
end
