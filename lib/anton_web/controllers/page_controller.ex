defmodule AntonWeb.PageController do
  use AntonWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
