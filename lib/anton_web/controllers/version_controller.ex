defmodule AntonWeb.VersionController do
  use AntonWeb, :controller

  alias Anton.Releases
  alias Anton.Releases.Version

  def index(conn, _params) do
    versions = Releases.list_versions()
    render(conn, "index.html", versions: versions)
  end

  def new(conn, _params) do
    changeset = Releases.change_version(%Version{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"version" => version_params}) do
    case Releases.create_version(version_params) do
      {:ok, version} ->
        conn
        |> put_flash(:info, "Version created successfully.")
        |> redirect(to: version_path(conn, :show, version))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    version = Releases.get_version!(id)
    render(conn, "show.html", version: version)
  end

  def edit(conn, %{"id" => id}) do
    version = Releases.get_version!(id)
    changeset = Releases.change_version(version)
    render(conn, "edit.html", version: version, changeset: changeset)
  end

  def update(conn, %{"id" => id, "version" => version_params}) do
    version = Releases.get_version!(id)

    case Releases.update_version(version, version_params) do
      {:ok, version} ->
        conn
        |> put_flash(:info, "Version updated successfully.")
        |> redirect(to: version_path(conn, :show, version))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", version: version, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    version = Releases.get_version!(id)
    {:ok, _version} = Releases.delete_version(version)

    conn
    |> put_flash(:info, "Version deleted successfully.")
    |> redirect(to: version_path(conn, :index))
  end
end
