defmodule AntonWeb.LayoutView do
  use AntonWeb, :view

  def active?(conn, path) do

    if starts_with_path?(current_path(conn, %{}), path) do
      "active"
    else
      ""
    end
  end

  defp starts_with_path?(request_path, "/") when request_path != "/", do: false
  defp starts_with_path?(request_path, to) do
    String.starts_with?(request_path, String.trim_trailing(to, "/"))
  end

  defp render_flash(type, message) do
    ~E"""
    <div class="alert alert-<%= type %>" role="alert">
      <%= message %>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    """
  end

  def show_flash(conn) do
    case get_flash(conn) do
      %{"info" => msg} ->
        render_flash("info", msg)
      %{"error" => msg} ->
        render_flash("danger", msg)
      _ ->
        ""
    end
  end

end
