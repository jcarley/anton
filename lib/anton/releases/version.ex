defmodule Anton.Releases.Version do
  use Ecto.Schema
  import Ecto.Changeset
  alias Anton.Releases.Version


  schema "versions" do
    field :name, :string
    field :notes, :string

    timestamps()
  end

  @doc false
  def changeset(%Version{} = version, attrs) do
    version
    |> cast(attrs, [:name, :notes])
    |> validate_required([:name, :notes])
  end
end
