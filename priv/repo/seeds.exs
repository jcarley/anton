# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Anton.Repo.insert!(%Anton.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.


Anton.Repo.delete_all Anton.Coherence.User

Anton.Coherence.User.changeset(%Anton.Coherence.User{}, %{name: "Jefferson Carley", email: "jeff.carley@gettyimages.com", password: "secret", password_confirmation: "secret"})
|> Anton.Repo.insert!
