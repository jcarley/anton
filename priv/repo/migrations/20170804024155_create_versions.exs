defmodule Anton.Repo.Migrations.CreateVersions do
  use Ecto.Migration

  def change do
    create table(:versions) do
      add :name, :string
      add :notes, :string

      timestamps()
    end

  end
end
