const webpack = require("webpack");
const path = require("path");

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const assetsDir = path.join(__dirname, ".");
const destDir = path.join(__dirname, "../priv/static");
const publicPath = "/";

module.exports = {
  entry: [
    path.join(assetsDir, 'js/app.js'),
    path.join(assetsDir, 'css/app.scss')
  ],
  output: {
    path: destDir,
    filename: 'js/app.js',
    publicPath
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['env'],
        },
      },
      {
        test: /\.s?css$/,
        use: ExtractTextPlugin.extract({
          use: "css-loader!sass-loader!import-glob-loader",
          fallback: "style-loader"
        })
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loader: "file-loader?name=fonts/[name].[ext]"
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "url-loader?limit=10000&mimetype=application/font-woff"
      },
      {
        test: /\.(png)$/,
        loader: "file-loader?name=images/[name].[ext]"
      },
      {
        test: /\.(svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "file-loader?name=images/[name].[ext]"
      }
    ],
  },
  resolve: {
    modules: [
      "node_modules",
      __dirname + "/js"
    ],
    alias: {
      phoenix_html: path.join(__dirname, "../deps/phoenix_html/priv/static/phoenix_html.js"),
      phoenix: path.join(__dirname, "../deps/phoenix/priv/static/phoenix.js")
    }
  },
  plugins: [
    new ExtractTextPlugin("css/app.css"),
    new CopyWebpackPlugin([
      { from: path.join(assetsDir, 'static/'), to: '../static' },
      { from: path.join(__dirname, "../deps/phoenix_html/priv/static/phoenix_html.js"),
        to: "js/phoenix_html.js" },
      { from: path.join(__dirname, "../deps/phoenix/priv/static/phoenix.js"),
        to: "js/phoenix.js" }
    ]),
  ],
}
