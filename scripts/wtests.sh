#!/usr/bin/env bash

# Reference: https://medium.com/the-missing-bit/test-driven-development-in-elixir-17287b003df2

fswatch -0 --latency=0.01 --one-per-batch lib test web | xargs -0 -n 1 mix test --stale test/
