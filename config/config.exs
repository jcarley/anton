# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :anton,
  ecto_repos: [Anton.Repo]

# Configures the endpoint
config :anton, AntonWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "LY4YHaFeOc4i3re0EEis6o/EPSEbQ3X775yPdBAVogFKCBBoxSW+aGA2pwzXuYAu",
  render_errors: [view: AntonWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Anton.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

# %% Coherence Configuration %%   Don't remove this line
config :coherence,
  user_schema: Anton.Coherence.User,
  repo: Anton.Repo,
  module: Anton,
  web_module: AntonWeb,
  router: AntonWeb.Router,
  messages_backend: AntonWeb.Coherence.Messages,
  logged_out_url: "/",
  email_from_name: "Your Name",
  email_from_email: "yourname@example.com",
  opts: [
    :unlockable_with_token,
    :trackable,
    :lockable,
    :recoverable,
    :authenticatable,
    :rememberable]

config :coherence, AntonWeb.Coherence.Mailer,
  adapter: Swoosh.Adapters.Sendgrid,
  api_key: "your api key here"
# %% End Coherence Configuration %%
